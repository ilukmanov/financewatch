var config = require('./config.js');


const AgregatorRoute = require('./controller/agregator').AgregatorRoute;
const SimpleHttp = require('./http/simplehttp');



new SimpleHttp()
    .on("(.*)", AgregatorRoute)
    .listen(config);

