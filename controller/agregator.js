var scrapeData = require('../http/scraper.js').JsonScraper;
var config = require('../config.js');
const ERROR_MESSAGE = "Failed to fetch data. Bad symbol or connection errors. ";


function ResultDataBuilder(baseUrl) {
    this.baseUrl = baseUrl;
    this.resultData = {};
    this.metaData = {};
    this.promises = [];

    this.indexKey = 0;
    this.scrape = function (url, key, saveTo, kind) {
        this.indexKey++;
        this.metaData[this.indexKey] = {saveTo: saveTo, kind: kind ? kind : "field", key: key};
        this.promises.push(scrapeData(this.baseUrl + url, this.indexKey));

        return this;

    }
    this.processResult = function (item) {
        if (item.key && this.metaData[item.key]) {

            var res = "not found";
            var md = this.metaData[item.key];
            if (md.kind == 'news' && item.data instanceof Array) {
                res = item.data[0][md.key]
            }
            if (md.kind == "field") {
                res = item.data[md.key]
            }
            if (!res)
                this.noFoundError(md.saveTo);
            else
                this.resultData[md.saveTo] = res;
        }
    }
    this.result = function () {

        return new Promise((resolve, reject) => {

            Promise.all(this.promises).then((results) => {
                results.forEach((item) => this.processResult(item))
                if (this.resultData.errors)
                    reject({error: ERROR_MESSAGE});
                else
                    resolve(this.resultData);
            }).catch((e) => reject({error: ERROR_MESSAGE}));

        })


    }

    this.noFoundError = function (saveTo) {
        this.error("`" + saveTo + "` not found");
    }


    this.error = function (error) {
        if (!this.result.errors) {
            this.result.errors = [];
        }
        this.result.errors.push(error);
    }
}


module.exports.AgregatorRoute = (symbol, resolve) => {
    return new ResultDataBuilder(config.apiurl + symbol)
        .scrape("/quote", "latestPrice", "latestStockPrice", "field")
        .scrape("/logo", "url", "companyLogoUrl", "field")
        .scrape("/news/last/1", "url", "latestArticleLink", "news")
        .result(resolve);
}