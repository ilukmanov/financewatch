# Run locally

1. Install [Node.js and npm](https://nodejs.org/)
1. Clone repository
<pre> 
git clone https://gitlab.com/finderto/financewatch.git
npm install
node app.js
</pre>

1. Visit [http://localhost:3000/[YOUR_SYMBOL](http://localhost:3000)
<pre>
curl http://localhost:3000/aapl
{"latestStockPrice":175.85,"companyLogoUrl":"https://storage.googleapis.com/iex/api/logos/AAPL.png","latestArticleLink":"https://api.iextrading.com/1.0/stock/aapl/article/8921854722133528"}

curl http://localhost:3000/bad_symbol
{"error":"Failed to fetch data. Bad symbol or connection errors. "}

</pre>

# Run via cli

1. Install [Node.js and npm](https://nodejs.org/)

<pre>
npm cli [YOUR SYMBOL]
</pre>
