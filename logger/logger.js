const fs = require("fs");
const formatter = require("./dtformatter");

function FileWriter() {
    this.writeLine = function (location, message) {
        fs.appendFile(location, message, (err) => {
            if (err) throw err;

        });
    }
}

function LogLineFormatter() {
    this.convertLine = function (line) {
        return formatter.formattedDateTime(new Date()) + " - " + line + "\t\n";
    }
}

function FileLocationFormatter() {
    this.convertLocation = function () {
        return formatter.formattedDate(new Date())+ ".basic.log";
    }
}


function Logger() {


    this.fileWriter = new FileWriter();
    this.lineLogger = new LogLineFormatter();
    this.fileLocationFormatter = new FileLocationFormatter();

    this.log = function (message) {

        var msg = this.lineLogger.convertLine(message);
        this.fileWriter.writeLine(this.fileLocationFormatter.convertLocation(), msg);
    }
}

module.exports = Logger;