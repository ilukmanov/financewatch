module.exports.formattedDateTime = function (date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    var strTime = hours + ':' + minutes + ':' + seconds;
    var month = date.getMonth() + 1;
    var day = date.getDay() + 1;
    var fullYear = date.getFullYear();

    return month + "/" + day + "/" + fullYear + " " + strTime;
};


module.exports.formattedDate = function (date) {
    var month = date.getMonth() + 1;
    var day = date.getDay() + 1;
    var fullYear = date.getFullYear();
    return fullYear + "." + month + "." + day;
};


