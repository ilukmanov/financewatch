const http = require('http');
const Logger = require('../logger/logger');
var url = require('url');
var logger = new Logger();

function SimpleResponse(resp) {
    this.resp = resp;

    this.err404 = function () {
        this.resp.writeHead(404);
        this.resp.end();
    }
    this.json = function (data) {
        this.resp.setHeader('Content-Type', 'application/json');
        this.resp.writeHead(200, '');
        this.resp.end(JSON.stringify(data));
    }
    this.err = function (msg) {
        this.resp.writeHead(200);
        this.resp.end(msg);
    }
}

function SimpleHttp() {

    this.qpMap = {};
}

SimpleHttp.prototype.on = function (qp, f) {
    this.qpMap[qp] = f;
    return this;
};


SimpleHttp.prototype.listen = function (config) {
    http.createServer((req, res) => {
        var simRes = new SimpleResponse(res);
        res.statusCode = 200;

        if (req.url == '/favicon.ico') {
            simRes.err404();
        } else {

            var found = false;

            for (var key in this.qpMap) {
                var reg=new RegExp(key);
                var result= reg.exec(req.url);
                if(result){
                    this.qpMap[key](result[1]).then((data) => {
                        simRes.json(data);
                        logger.log(req.url + "OK")
                    }).catch((data) => {
                        simRes.json(data);
                        logger.log(req.url + " some data missing")
                    });
                    found = true;

                    break;
                }
                console.log("Key"+req.url+key+": ",result);
            }

            if(!found){
                logger.log(req.url + ": bad request");
                simRes.err404();
            }


        }
    }).listen(config.port, config.hostname, () => {
        console.log(`Server running at http://${config.hostname}:${config.port}/`);
    });
}


module.exports = SimpleHttp;
