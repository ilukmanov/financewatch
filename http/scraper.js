var http = require('http');
var url = require('url');
var https = require('https');


module.exports.JsonScraper = function (urlRequest, key) {
    return new Promise((resolve, reject) => {

        var urldata = url.parse(urlRequest);
        var isHttps = urldata.protocol.indexOf("https") !== -1;
        var httpOptions = {
            host: urldata.hostname,
            port: isHttps ? 443 : 80,
            path: urldata.path
        };


        var req = (isHttps ? https : http).request(httpOptions, (res) => {
            res.setEncoding('utf8');
            let rawData = '';
            var isJson = res.headers['content-type'].indexOf('application/json') !== -1;

            res.on('data', (chunk) => {
                rawData += chunk;
            });
            res.on('end', () => {
                if (!isJson) {
                    reject("data is not json", rawData);
                } else {
                    resolve({
                        key: key,
                        data: JSON.parse(rawData)
                    });
                }
            });
        });


        req.on('error', (e) => {
            reject(e);
        });
        req.end();


    });
};